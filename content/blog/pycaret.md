---
title: "Auto ML(pycaret)활용하여 손쉽게 머신러닝 사용하기"
date: 2024-01-28T14:00:00+09:00
# weight: 1
# aliases: ["/first"]
categories: "blog"
tags: ["AutoML","pycaret", "머신러닝", "자동" ]
author: "Me"
# author: ["Me", "You"] # multiple authors
srcset: true
showToc: true
TocOpen: true
draft: false
hidemeta: false
comments: true
description: "Auto ML을 활용하여 손쉽게 ML을 적용하는 방법"
# canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: false
cover:
    image: "blog/pycaret/pycaret_cover.png" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
# editPost:
#     URL: "https://gitlab.com/choineral/choineral.gitlab.io/-/tree/main/content"
#     Text: "Suggest Changes" # edit text
#     appendFilePath: true # to append file path to Edit link
---

## Introduction
많은 AI 경진대회를 나가면서 느낀점은 머신러닝에 대한 이해도 중요하지만, 데이터 전처리와 모델링을 하는데 시간을 얼마나 쏟느냐도 매우 중요한 포인트라는 점입니다.

따라서 지금까지 생각만하고 사용하지 못했던 Auto ML읉 사용해 봐야겠다고 생각했습니다. 왜냐하면 Domain 지식과 EDA를 통해 얻은 insight로 데이터를 처리하는 것이 내가 노력을 해야 하는 부분이고, 그 이후 모델링을 위한 전처리와 좋은 모델을 고르거나 Feature Selection 하는 부분은 컴퓨터가 더 잘 해낼 수 일이라고 생각했기 때문입니다.

Auto ML을 사용가능 여러가지 Tool중에 예전에 수업에서도 잠깐 배웟던 **Pycaret**을 사용해 보려 합니다.

---

## Pycaret 사용법
일단 사용한 결론부터 말씀드리면 **너무나 간편하다** 입니다. 제 생각보다 사용법이 너무 쉽고 간편하고 결과도 잘 나와서 놀랬습니다. 예전에 SKAI 경진대회에서 사용했던 데이터로 확인해 보았습니다. (데이터에 대한 내용은 공유할 수가 없어, 사용했던 코드와 결과 공유 드립니다)

### [STEP1] Data Preprocessing
여기서 말하는 데이터 전처리는, 모델링을 하기 위해서 필요한 전처리를 의미하는 것입니다. 결측치 처리, polynomial feature 여부, feature selection, remove outlier등 우리가 생각했던 모든 전처리를 간단한 코드 한줄로 처리할 수 있습니다. 제가 사용한 코드와 hyperpara에 대한 설명입니다.

    clf = setup(train_xy, target="CHURN",profile=True,train_size=0.8,session_id=777,
            imputation_type='iterative', polynomial_features=True, remove_outliers=True,
            fold = 5,
            remove_multicollinearity=True)


- data : Input data를 입력해주시면 됩니다. Train, Test를 별도로 분리하지 않고 입력할 수도 있어 편리했습니다.

- target : 결과 column을 입력해 주시면 됩니다. (Y값)
- session_id : random_seed로 잘 알려진 hyperpara 입니다.
- normalize : 데이터에 정규화를 할 것인지 True/False로 선택합니다.
- normalize_method : normalize를 True인 경우 어떤 방식으로 정규화를 진행할 것인지를 정합니다.
 ('minmax','maxabs','robust')
- transformation : Power Transformation을 통해서 데이터 샘플들의 분포가 가우시안 분포(정규 분포)에 더 가까워지도록 처리해주는 과정입니다.
- fold_strategy : Fold starategy전략을 선택해서 설정할 수 있는 하이퍼파라미터 입니다. pycaret은 기본적으로 10-fold Cross-Validation을 수행합니다.
- fold : fold 개수를 지정합니다. 전 시간 단축을 위해 5번만 fold하도록 설정 했습니다.
- remove_multicollinearity : 다중공선성을 제거할지 여부를 결정합니다.

![](pycaret_data_preprocessing.png)

### [STEP2] model comparision
좋은 모델을 선택하는데 도움을 주는 함수가 Pacaret에 존재합니다. compare_models() 함수를 활용하면 되는데, 각 모델별로 비슷한 조건으로 데이터를 넣고 돌려서 높은 score가 나오는 순서대로 model을 정리해줍니다. 원하는 score에 맞춰서 sorting 가능하고, 저같은 경우에는 F1-score가 중요하기 때문에 그 점수에 맞춰서 sorting 했습니다.

    mymodels = compare_models(sort="F1",n_select=3)

전처리 조건만 잘 주면 여기서부터는 정말 코드가 더 쉽습니다. 제가 사용한 hyperpara에 대한 설명만 적고, 나머지는 docs 참고해주시면 될것 같습니다.

- sort : 어떤 score로 sort할 것인지 결정합니다.
- n_select : 이 값만큼 model을 return합니다. 저같은 경우 상위 3개 모델이 mymodels에 담깁니다.

![](pycaret_model_compare.png)

다음은 pycaret이 기본적으로 비교하는 model의 종류입니다.

![](pycaret_models.png)

### [STEP3] model hyperpara tuning
제가 선택한 상위 3개 모델에 대해서 hyperpara tuning을 진행합니다. tuning을 진행한 결과가 original model보다 점수가 낮을 시, original model을 자동으로 선택해주는 것도 너무 편했습니다.

    mymodels_tuned = [tune_model(model,optimize="F1") for model in mymodels]

### [STEP4] model blending
모델을 stact하거나 voting하거나 ensamble하는 등의 모델을 합쳐서 시너지를 낼 수 있는 여러가지 함수가 존재합니다. 그 중 저는 상관관계가 낮은 model들을 ensamble할 수 있는 blend_models 함수를 사용해 봤습니다. 이또한 편리하게 진행 가능합니다.

    mymodel_blended = blend_models(mymodels_tuned, optimize="F1")

### [STEP5] model evaluation
모델 결과를 시각화하여 볼 수 있습니다. 간단한 함수 하나로 보통 ML을 진행할 때 확인하는 거의 모든 부분을 시각화로 확인할 수 있습니다. blended model의 경우 확인할 수 있는 시각화가 많이 없어, tuned model을 시각화로 확인해 봤습ㄴ디ㅏ.

    evaluate_model(mymodels_tuned[0])

![](pycaret_visual.png)

## Conclusion
설명을 보시면 알겠지만 사실 데이터 전처리 하는 부분의 함수에 대한 공부만 조금 진행하면 나머지는 사용하기가 너무 쉽습니다. 그리고 제가 직접 전처리, 모델링등을 진행해서 나온 결과와 pycaret을 이용한 결과를 비교했을 때 F1-score가 0.33에서 0.508로 증가한 것을 확인할 수 있었습니다. 저는 지금까지 데이터에 대한 처리로 인해서 점수를 비약적으로 상승 가능하고, modeling을 통해서는 크게 증가시킬 수 없다고 생각했었는데, 이번 결과를 통해 modeling도 정말 중요하다고 생각이 들었습니다.

이처럼 modeling에 대해서 어느정도 지식이 있으신 분들이 사용하면 매우 편리하게 ML 결과를 비교할 수 있다는 점에서 크게 강점이 있는 것 같습니다. 앞으로 적극 활용할 수 있을 것 같아 좋습니다.

---

## 참고
- https://mz-moonzoo.tistory.com/5