---
title: "Hugo와 Gitlab Pages로 정적 웹페이지 구현하고 배포하기"
date: 2023-09-03T13:32:00+09:00
# weight: 1
# aliases: ["/first"]
categories: "blog"
tags: ["hugo","gitlab","blog","gitlab pages","블로그"]
author: "Me"
# author: ["Me", "You"] # multiple authors
srcset: true
showToc: true
TocOpen: true
draft: false
hidemeta: false
comments: true
description: "정적 웹사이트를 Hugo와 Gitlab으로 구성해보자"
# canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: false
cover:
    image: "blog/hugo-blog-start/hugo-cover.png" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
# editPost:
#     URL: "https://gitlab.com/choineral/choineral.gitlab.io/-/tree/main/content"
#     Text: "Suggest Changes" # edit text
#     appendFilePath: true # to append file path to Edit link
---

## Introduction
많은 글은 아니지만, tistory, notion을 통해 블로그 글을 작성해왔고 불편함을 느껴서 새로운 웹페이지 배포 방법을 찾다 Hugo라는 tool을 발견했습니다. Hugo를 내 새로운 posting tool로 선택하게 된 이유는 3가지입니다.

1. 포스팅 글을 저장하고 옮기고 수정하는데 용이하다
2. 내가 사용하던 Tool에 비해 빠르고 간결하다
3. 처음에는 낯설지만 적응하면 더 편하게 글을 작성할 수 있다

이외에도 결정하게 된 여러가지 이유가 있지만 가장 큰 이유는 위 세가지였습니다. 생각보다 hugo 환경으로 블로그를 작성하고 배포하기까지의 과정이 쉽지 않았기 때문에 그 내용을 공유하고자 합니다.

---

## Hugo 설치부터 Gitlab을 통한 웹페이지 배포까지
### [STEP1] Gitlab에서 배포 용 프로젝트 만들기
Hugo blog를 배포하기 위한 Project를 만드는 것이 첫번째입니다.


gitlab에서는 **[username].gitlab.io**로 생성한 project를 gitlab page로 사용했을 때 도메인을 해당 이름으로 그대로 제공하는 기능이 있습니다.


새로운 project name으로 만들게 되면 **[username].gitlab.io/[proejct name]** 으로 생성됩니다.

![](gitlab-proejct-create.png)

### [STEP2] Hugo 설치

제 컴퓨터는 Mac이기 때문에 brew로 설치했습니다.

    brew install hugo

다른 OS인 경우 hugo 홈페이지에 들어가면 방법이 나와있습니다. (https://gohugo.io/)

### [STEP3] Hugo 웹페이지 폴더 생성 및 Gitlab 저장소 연결

    hugo new site blog

blog라는 이름의 hugo 사이트 디렉토리가 생성됩니다.

생성된 디렉토리를 **STEP1**에서 생성했던 gitlab 저장서와 연결해 줘야합니다.

    git init
    git remote add origin <본인 GIT 저장소>

### [STEP4] 테마 넣어주기

처음 사용해보는 것이기 때문에 hugo에서 제공하는 theme를 사용하면 좋겠다고 생각했고,
기본적으로 제공해 주는 theme중 PaperMod theme를 선택해서 submodule로 넣어주었다.

theme 별 사용하는 방법이 다를 수 있으므로, 원하는 theme의 사용 설명서를 잘 읽어보길 바랍니다.

저는 PaperMod의 Installation 문서를 참고하여 사용할 theme를 구성했습니다.\
https://github.com/adityatelange/hugo-PaperMod/wiki/Installation

    git submodule add --depth=1 https://github.com/adityatelange/hugo-PaperMod.git themes/PaperMod
    git submodule update --init --recursive

### [STEP5] config.yml 구성하기 (PaperMod 테마 사용 설정)

Papermod 문서를 살펴보면, 테마를 사용하기 위해서는 config.yml을 생성해 줘야 합니다.

    baseURL: "https://choineral.gitlab.io"
    theme: PaperMod

제 기준에서 다음과 같이 파일을 구성하면 PaperMod 테마를 적용할 수 있습니다.

### [STEP6] gitlab-ci.yml

gitlab의 CD/CI 기능을 통해 Page를 배포 할 것이기 때문에, 해당 파일을 구성해 줘야합니다.

    image: monachus/hugo
    variables:
    GIT_SUBMODULE_STRATEGY: recursive
    pages:
    script:
        - hugo
    artifacts:
        paths:
        - public
    only:
        - main

### [STEP7] Gitlab Pages 배포/게시
그동안 구성한 것들을 저장소에 커밋하고 Push 하겠습니다.

    git add .
    git commit -m "Initial commit"
    git push -u origin main

이렇게만 하면 끝일 줄 알았는데 여기서 많이 헤맸습니다.\
Gitlab Page를 배포하기 위해서는 runner 관련 몇가지 사전 준비가 필요합니다.

1. Shared runners 끄기

    먼저 Settings > CI/CD Settings 메뉴에서 Shared runners를 Off해야 runner가 작동되었습니다.

    ![](shared-runner-option-off.png)

2. Runner 생성하여 실행해주기

    runner를 통해 proejct가 배포될 수 있도록 구성해야합니다.\
    저도 이부분에 대한 자세한 내용은 모르지만, Docker내에 구성하여 주로 사용하는 것으로 보입니다.

    ![](new-project-runner.png)

    먼저 그림에서 New project runner 버튼을 통해 runner를 생성하면 gitlab runner를 설치하고 생성된 project runner를 등록하라는 페이지가 나옵니다. 본인의 환경에 따라 설치 및 register 해주시면 됩니다.

    저는 Mac 사용자이기 떄문에 아래 명령어를 통해 gitlab-runner를 설치했습니다.

        # Download the binary for your system
        sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-arm64

        # Give it permission to execute
        sudo chmod +x /usr/local/bin/gitlab-runner

        # Create a GitLab Runner user
        sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

        # Install and run as a service
        sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
        sudo gitlab-runner start

이렇게 두가지 사전준비가 끝나면 성공적으로 배포가 가능한 상태가 됩니다.

### [STEP8] 결과 확인하기
잘 배포가 되었는지 확인해 보았습니다.

https://choineral.gitlab.io/

![](myhome.png)

![](myblog.png)

---

## Conclusion
Hugo로 첫 포스팅 기념(?)으로 어떻게 블로그를 만들고 배포했는지에 대해 글을 작성하게 되었습니다. 사용하면 할수록 편리하고 좋은 정적 웹사이트 배포 방식이 아닐까 싶습니다.

블로그 글을 쓰고 올리는 과정은 선택하신 theme에 따라 다르기 때문에, 사용하시려는 theme 홈페이지에 들어가서 참고하시게 좋을 것 같고, 아나면 아예 새로 만들어 버리는 것도 재미있어 보입니다.

---

## 참고
- https://insight.infograb.net/blog/2021/04/15/gitlab-pages-hugo-blog/