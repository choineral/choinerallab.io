---
title: "통상적이지 않은 이미지 분류에 대한 고찰-2"
date: 2024-01-02T14:00:00+09:00
# weight: 1
# aliases: ["/first"]
categories: "blog"
tags: ["image-processing","딥러닝","deep-learning",
"classification","normalization","정규화"]
author: "Me"
# author: ["Me", "You"] # multiple authors
srcset: true
showToc: true
TocOpen: true
draft: false
hidemeta: false
comments: true
description: "이미지 detection & classification 기술을 통해 부족한 이미지셋을 극복해보자"
# canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: false
cover:
    image: "blog/image-processing-detect-and-classfication/object_detection.jpeg" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
# editPost:
#     URL: "https://gitlab.com/choineral/choineral.gitlab.io/-/tree/main/content"
#     Text: "Suggest Changes" # edit text
#     appendFilePath: true # to append file path to Edit link
---
> 이포스트는 이미지를 딥러닝으로 분류할 때 데이터 셋이 적고
> 통상적으로 퍼져있는 이미지 데이터셋을 사용하지 못할 때 유용한 방법에 대한 포스트입니다.

# Introduction
앞서 서술한 "통상적이지 않은 이미지 분류에 대한 고찰" 포스트에서 명품 손목시계 이미지 분류를 진행할 때 적은 이미지 셋을 `CROP`, `Transfer Learning(전이학습)`등을 통해 한계를 극복한 방법을 서술했습니다. 이번 포스트에서는 분류가 잘 안된 이미지를 분석하고 해결방안을 찾은 사례에 대해서 소개하고자 합니다.

# 왜 분류가 제대로 되지 않았는가?
앞선 model도 분명 괜찮을 정도의 성능이 나왔지만, 더 높은 성능을 위해서 데이터 분석이 필요하다고 느꼈습니다. 따라서 가장 먼저 분류가 제대로 안된 이미지를 확인 해 보았습니다.

![](희미한시계.png)

시계에서 어떤 점이 느껴지셨나요? 네 맞습니다. 화질이 희미하고 빛번짐이 심합니다. 실제 이미지 데이터는 이처럼 사용자의 사진 촬영 환경이나, 사진 촬영 도구에 따라 왜곡이 심해지게 되어 컴퓨터가 제대로 시계 분류를 하지 못하게 됩니다.

# Image Normalization

![](geoffrey_hinton.png)

이러한 문제를 해결하는 가장 기본적인 방법 중에 하나가 `Normalization` 기법 입니다. 이미지 각 픽셀에 해당하는 컬러가 각 RGB 채널에서 정규분포를 가지도록 만들어주는 기법인데, 이를통해 좀더 명확한 색상을 가지게 하여 이미지 분류가 잘 되도록 하는 것입니다.

제가 실제로 적용한 방법은, 이미지를 train, test셋으로 나눌 때 함수를 통해 normalization이 적용 되도록 하였습니다.

    def custom_normalization(image):
        r, g, b = cv2.split(image)
        b_norm = cv2.normalize(b, None, 0, 1, cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        g_norm = cv2.normalize(g, None, 0, 1, cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        r_norm = cv2.normalize(r, None, 0, 1, cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        img = cv2.merge([r_norm, g_norm, b_norm])
        return img

    data_gen = ImageDataGenerator(
        preprocessing_function=custom_normalization, validation_split=0.2
    )

## 실제 이미지 비교

![](normalization_비교.png)

왼쪽이 적용 전, 오른쪽이 적용 후 사진입니다. 적용 전후를 비교해보면 명확하게 오른쪽 사진이 선명하게 보이는 것을 확인하실 수 있습니다.

# 평가

![](적용전후비교.png)

위 그래프는 특정 손목 시계 이미지를 넣었을 때, 몇순위만에 손목 시계 종류를 맞추었는지에 대한 그래프입니다. normalization 적용 후 1순위로 맞출 확률이 10%가량 상승한 것을 확인할 수 있었습니다.

# Conclusions
실제 제가 진행하고 있는`명품 손목시계 분류 Project`는 이미지 데이터 셋이 적은 문제를 가지고 있었고,
덕분에 어떻게하면 적은 데이터 셋으로 제대로된 분류를 할 수 있을까에 대한 많은 고민을 하였습니다. 그 결과 만족할만한 수준의 분류 성능을 만들어내어 실제 User에게 제공할 수 있게 되었습니다. 이미지 프로세싱에 대해 실제 데이터를 다뤄보고 적용해본 좋은 경험이 었다고 생각합니다.
