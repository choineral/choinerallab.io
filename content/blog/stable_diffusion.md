---
title: "stable diffusion으로 AI 이미지 만드는 방법"
date: 2024-02-24T15:04:32+09:00
categories: "blog"
tags: ['Stable Diffusion', 'AI 이미지 생성', '확률론적 모델', '이미지 초기화', 'Diffusion Step', ' Hyper-parameters', ' Python 코드', '이미지 생성', '안정성', '노이즈 침식', ' 다양한 스타일', 'web-ui']
author: "Me"
srcset: true
showToc: true
TocOpen: true
draft: false
hidemeta: false
comments: true
description: "Stable Diffusion을 이용한 AI 이미지 생성 방법과 예제 코드를 소개합니다."
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: false
cover:
    image: "blog/stable_diffusion/cover.png" # image path/url
    alt: "https://oaidalleapiprodscus.blob.core.windows.net/private/org-wUk9A9f8X6DXncq7lbEattcL/user-cC80TbCUIcXCwVkY1ItwCCuA/img-VGSEzpVJA4ofKa9wQ5kVAk7q.png?st=2024-02-08T05%3A04%3A18Z&se=2024-02-08T07%3A04%3A18Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2024-02-07T20%3A03%3A09Z&ske=2024-02-08T20%3A03%3A09Z&sks=b&skv=2021-08-06&sig=82YCMRkQYyW7xNhfBu3Qyg6%2BtNNULqlQ6j2VTD8yAGQ%3D" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
---
# Web Stable Diffusion으로 AI 이미지 만드는 방법

이미지 생성은 인공지능 분야에서 많은 연구와 관심을 받고 있는 분야입니다. 그 중에서도 Stable Diffusion은 이미지 생성에 효과적인 방법 중 하나입니다. Stable Diffusion은 이미지 생성에 확률론적인 방법을 사용하여 안정적이고 다양한 스타일의 이미지를 생성할 수 있습니다. 이 블로그에서는 Stable Diffusion을 이용하여 AI 이미지를 만드는 방법과 그 과정을 소개하겠습니다.

## Stable Diffusion 소개

Stable Diffusion은 이미지 생성에 사용되는 확률론적 모델 중 하나로, 이미지의 점진적인 변화를 통해 원하는 이미지를 생성하는 방식입니다. 이 방법은 이미지를 점차적으로 노이즈로 침식하고, 노이즈를 확산시켜 원하는 이미지를 얻을 수 있습니다. Stable Diffusion은 이미지 생성에 안정성을 보장하는 동시에 다양한 스타일의 이미지를 생성할 수 있어 많은 연구자들에게 인기를 얻고 있습니다.

다양한 방식으로 Stable Diffusion을 사용할 수 있는데, 저는 가장 사람들에게 친숙하고 쉽게 사용할 수 있는 web-ui를 활용한 사용법을 소개 하겠습니다.

## Stable Diffusion 알고리즘

![](latent-diffusion.png)

Stable Diffusion 알고리즘 구조는 Latent Diffusion 구조를 따르고 있고, 대략적으로 다음과 같은 과정으로 이루어집니다.

1. 이미지 초기화: 생성하고자 하는 이미지를 임의의 노이즈로 초기화합니다.

2. Diffusion Step: 초기화된 이미지에 확산 과정을 적용합니다. 이 과정에서 이미지는 점차적으로 변화하며, 안정성을 보장합니다.

3. 생성된 이미지 확인: Diffusion Step을 마친 후 최종 생성된 이미지를 확인합니다. 생성된 이미지가 원하는 스타일과 형태를 갖추었는지 확인해야 합니다.

4. Hyper-parameters 조정: 알고리즘의 hyper-parameters를 조정하여 원하는 결과를 얻을 수 있도록 합니다.

## Stable Diffusion web-ui 사용법

Stable Diffusion을 web-ui로 즐길 수 있는 방법을 소개하겠습니다. 특히 저는 Mac M2를 사용하고 있기 때문에 그 기준으로 설명 드리겠습니다.

### 01. brew로 필요한 package 설치
brew를 이용해서 필요한 package를 설치합니다.

`brew install cmake protobuf rust python@3.10 git wget`

홈페이지에서 설명은 저렇게 하라고 나와있지만, 대부분 가상환경을 사용하고 있을거라고 생각합니다. python@3.10은 제외하고 설치해주시면 됩니다.

### 02. web-ui 버전 repository 가져오기
가장 유명한 web-ui stable diffusion은 https://github.com/AUTOMATIC1111/stable-diffusion-webui 입니다. repository를 원하는 경로에 clone 해주세요

`git clone https://github.com/AUTOMATIC1111/stable-diffusion-webui YOUR-PATH`

### 03. model을 넣어주세요
사용할 모델을 `{YOUR-PATH}/models/Stable-diffusion`에 넣어줘야 합니다. https://github.com/AUTOMATIC1111/stable-diffusion-webui/wiki/Installation-on-Apple-Silicon#downloading-stable-diffusion-models 여기 경로에서 Mac에서 구동하는 model을 다운받을 수도 있는데, .ckpt파일은 악성코드에 취약하기 때문에 아래 경로에서 .safetensors 형태의 파일로 받으시길 바랍니다.

`https://huggingface.co/runwayml/stable-diffusion-v1-5/tree/main`

### 04. web-ui 실행

project 최상위 폴더로 돌아가서 `./webui.sh` 실행해주시면 처음에 이것저것 설치한 이후 localhost web-ui가 실행됩니다. 이제 맥의 리소스를 활용하여 TTL 하실 수 있습니다!

![](result.png)


## 결론

Stable Diffusion은 AI 이미지 생성에 효과적인 방법 중 하나입니다. 이미지 생성을 위해 확률론적인 방법을 사용하여 다양한 스타일의 이미지를 생성할 수 있습니다. 위에서 소개한 방법과 예제 코드를 참고하여 여러분도 AI 이미지 생성에 도전해보세요. Stable Diffusion을 통해 원하는 이미지를 얻을 수 있을 것입니다.

## 참고
- https://coconuts.tistory.com/1016
- https://github.com/AUTOMATIC1111/stable-diffusion-webui?tab=readme-ov-file
- https://huggingface.co/runwayml/stable-diffusion-v1-5/tree/main%E3%85%88
- https://github.com/AUTOMATIC1111/stable-diffusion-webui/wiki/Installation-on-Apple-Silicon